#!/usr/bin/env python
# coding: utf-8

# In[49]:



# In[1]:


import pandas as pd
#import os
import numpy as np
import scipy.stats as stats
import math
from scipy.special import erf
import time
#import random
from scipy.optimize import minimize
from scipy.optimize import check_grad
import multiprocessing as mp
import warnings
from functools import partial 
#from statistics import mean 
import sys
#import email.utils
#import calendar
#import datetime
#import utm
import analyze_mcmc
from scipy.stats import multinomial
import collections

# import pixiedust
# path of packages - /home/manisha/anaconda3/lib/python3.6/site-packages/scipy/optimize


# In[2]:
#
old_stdout = sys.stdout
#log_file = open("log_diagnostic_" + str(datetime.datetime.now()) + ".log","w")
log_file = open("log_diagnostic_rerun_after_simulation_our_model3" + ".log","w")
# log_file = open("hptuning_4.log","w")
sys.stdout = log_file
warnings.filterwarnings('ignore')
np.set_printoptions(threshold=1000)

# In[3]:

#os.chdir('../Dataset/')
#old_stdout = sys.stdout
#log_file = open("log_file_600m_PostReg" + str(datetime.datetime.now()) + ".log","w")
#sys.stdout = log_file

# In[4]:

print(" ============= WORKING ON SIMULATED DATASET ====================")
#tky_checkins_df_complete = pd.read_csv("/home/manisha/Documents/git/semanticannotationhawkesprocess/SemanticAnnotation/Dataset/KaggleDataset/SimulatedDataset_100Missing_1.csv", na_values='', 
#                                       dtype={'userId':int, 'TimeStamp':float, 'Latitude':float, 'Longitude':float, 'venueCategory':object,'CategoryId':float})
tky_checkins_df_complete = pd.read_csv("/home/manisha/Documents/git/semanticannotationhawkesprocess/SemanticAnnotation/Dataset/KaggleDataset/SimulatedDataset_100Missing_3.csv", na_values='')

#tky_checkins_df_complete.index = range(tky_checkins_df_complete.shape[0])
#print(tky_checkins_df_complete.index)

# In[5]:
#value = tky_checkins_df_complete['utcTimestamp'].apply(lambda x: datetime.datetime.fromtimestamp(calendar.timegm(email.utils.parsedate(x))))
#value = pd.Timestamp(tky_checkins_df_complete['utcTimestamp'])
#tky_checkins_df_complete['TimeStamp'] = value

#latitude = []
#longitude = []

#for idx, each_row in tky_checkins_df_complete.iterrows():
#    lat = each_row['latitude']
#    long = each_row['longitude']
#    u = utm.from_latlon(lat, long)
#    latitude.append(u[0]/1e2)
#    longitude.append(u[1]/1e8)
#
#tky_checkins_df_complete['latitude'] = latitude
#tky_checkins_df_complete['longitude'] = longitude
#print(tky_checkins_df_complete['longitude'])
#print(tky_checkins_df_complete['latitude'])

# In[6]:


tky_checkins_df_complete['IndexRow'] = tky_checkins_df_complete.index

tky_checkins_df_complete.sort_values(['userId', 'TimeStamp'], inplace=True)
tky_checkins_df_complete.reset_index(drop=True, inplace=True)
print(tky_checkins_df_complete)

# In[7]:


print(tky_checkins_df_complete.shape)
col_names = tky_checkins_df_complete.columns
for each_col in col_names:
    print("Number of unique values of " + str(each_col) + "---" + str(tky_checkins_df_complete[each_col].unique().size))
num_values = tky_checkins_df_complete.shape[0]
print("rows of checkins_df " + str(num_values))
#del tky_checkins_df_complete['CategoryId']
#del tky_checkins_df_complete['timezoneOffset']


# In[8]:


## -1 for '' missing category
num_unique_cat = tky_checkins_df_complete['venueCategory'].unique().size - 1
print("Number of unique categories " + str(num_unique_cat))

num_missing_cat = tky_checkins_df_complete['venueCategory'].isna().sum()
print("Number of missing values " + str(num_missing_cat))

num_users = tky_checkins_df_complete['userId'].nunique()
print(num_users)


# In[9]:


missing_values_index = []
tky_checkins_df_complete.fillna('' , inplace=True)

for index, row in tky_checkins_df_complete.iterrows():
    if row['venueCategory'] == '':
        missing_values_index.append(index)
        
print(missing_values_index)
print(len(missing_values_index))
    


# In[10]:


#categories = []
#for _, row in tky_checkins_df_complete.iterrows():
#       if row['venueCategory'] not in categories and row['venueCategory'] != '':
#            categories.append(row['venueCategory'])
#print(categories)
categories = list(tky_checkins_df_complete.venueCategory.unique())
categories.remove('')
print(categories)

# In[11]:


categories_id_col = []
for idx, each in tky_checkins_df_complete.iterrows():
    if each['venueCategory'] == '':
        categories_id_col.append('')
    else:
        categories_id_col.append(categories.index(each['venueCategory']))
tky_checkins_df_complete['CategoryId'] = categories_id_col
#print(tky_checkins_df_complete.head(10))

#tky_checkins_df_complete.fillna(num_unique_cat +1, inplace=True)
#print(tky_checkins_df_complete.CategoryId)
#tky_checkins_df_complete.CategoryId = tky_checkins_df_complete.CategoryId.astype(int)
#tky_checkins_df_complete.index = tky_checkins_df_complete.index.astype(int)
#print(tky_checkins_df_complete.CategoryId)

# In[12]:


actual_output_df = pd.read_csv("/home/manisha/Documents/git/semanticannotationhawkesprocess/SemanticAnnotation/Dataset/ActualOutputSimulatedDataset3.csv")
actual_output = actual_output_df['ActualOutput'].tolist()
#print(actual_output)
#print(len(actual_output))


# In[13]:


userIds = []
dataframe_collection = []
for each in tky_checkins_df_complete['userId'].unique():
    df = tky_checkins_df_complete[tky_checkins_df_complete['userId'] == each]
    df.sort_values(['TimeStamp'], inplace=True)
    df = df.reset_index(drop=True)
    userIds.append(each)
    dataframe_collection.append(df)
    

# In[15]:

def pre_compute_spatial_kernel(dataframe_collection, h):
    start = time.time()
#     num_events = tky_checkins_df_complete.shape[0]
    num_events = tky_checkins_df_complete.shape[0]
    precomp_spatial_kernel = np.zeros((num_events, num_events))
    precomp_spatial_integral = np.zeros((num_events - 1, num_events - 1))
    
    for idx, tky_checkins_df_each in enumerate(dataframe_collection):
#         print(tky_checkins_df_each)
#         print("User ID being processed " + str(userIds[idx]))
        size = tky_checkins_df_each.shape[0]
       
        tky_checkins_df = tky_checkins_df_each.reset_index()
        
        for i, each_row in tky_checkins_df.iterrows():
            
            index = int(each_row['IndexRow'])  
#            print("IndexRow " + str(index))
            if i == 0:

                latitude_int = (erf(tky_checkins_df['latitude'][1]) - erf(tky_checkins_df['latitude'][0]))*math.sqrt((math.pi*h)/2)
                longitude_int = (erf(tky_checkins_df['longitude'][1]) - erf(tky_checkins_df['longitude'][0]))*math.sqrt((math.pi*h)/2)

                precomp_spatial_integral[index][0] = latitude_int*longitude_int
                continue

            latitude_n = tky_checkins_df['latitude'][i]
            longitude_n = tky_checkins_df['longitude'][i]

            if i != size - 1:
                max_latitude = tky_checkins_df['latitude'][i + 1]
                max_longitude = tky_checkins_df['longitude'][i + 1]

            latitude_k = tky_checkins_df['latitude'][0:i]
            longitude_k = tky_checkins_df['longitude'][0:i]
#             print(np.exp(-(((latitude_n - latitude_k)**2 + (longitude_n - longitude_k)**2)/2*h)))
            precomp_spatial_kernel[index] = np.pad(np.exp(-(((latitude_n - latitude_k)**2 + 
                                                        (longitude_n - longitude_k)**2)/2*h)), (0,num_events - i), 'constant')

            #precomp_spatial_kernel[index] = latitude_k*longitude_k

            if i == size - 1:
                break

            latitude_integral = (erf(max_latitude - latitude_k) - erf(latitude_n - latitude_k))*math.sqrt((math.pi*h)/2)
            longitude_integral = (erf(max_longitude - longitude_k) - erf(longitude_n - longitude_k))*math.sqrt((math.pi*h)/2)
#             print(latitude_integral*longitude_integral)
            precomp_spatial_integral[index] = np.pad(latitude_integral*longitude_integral, (0,num_events - i - 1), 'constant' )
            #precomp_spatial_integral[index] = latitude_integral*longitude_integral

    print("Time taken by pre_compute_spatial_kernel is {} seconds".format((time.time() - start)))
    print(precomp_spatial_kernel.shape)
    print(precomp_spatial_integral.shape)
    print(precomp_spatial_kernel)
    print(precomp_spatial_integral)
    
    return precomp_spatial_kernel, precomp_spatial_integral
    
# precomp_spatial_kernel, precomp_spatial_integral = pre_compute_spatial_kernel(dataframe_collection, 1e-1)


# In[16]:


def pre_compute_temporal_kernel(dataframe_collection, eta):
    start = time.time()
    #num_events = checkins_df.shape[0]
    num_events = tky_checkins_df_complete.shape[0]
    precomp_temporal_kernel = np.zeros((num_events, num_events))
    precomp_temporal_integral = np.zeros((num_events - 1, num_events - 1))
    
    for idx, tky_checkins_df_each in enumerate(dataframe_collection):
#         print("User ID being processed " + str(userIds[idx]))
        size = tky_checkins_df_each.shape[0]

        tky_checkins_df = tky_checkins_df_each.reset_index()
        
        for i, each_row in tky_checkins_df.iterrows():
            
            index_n = int(each_row['IndexRow'])  
#             print("IndexRow " + str(index_n))

            if i == 0:
                
                precomp_temporal_integral[index_n][0] = (np.exp(-1*eta*tky_checkins_df['TimeStamp'][1]) -
                                                    np.exp(-1*eta*tky_checkins_df['TimeStamp'][0]))/-eta
                continue

            timestamp_n = tky_checkins_df['TimeStamp'][i]
            timestamp_k = tky_checkins_df['TimeStamp'][0:i]
            
            if i != size - 1:
                max_timestamp = tky_checkins_df['TimeStamp'][i + 1]

    #         print(timestamp_k.apply(lambda x:(timestamp_n - x).seconds/60))
            time_diff = np.exp(-1*eta*timestamp_k.apply(lambda x:(timestamp_n - x)))      
            time_diff_max = np.exp(-1*eta*timestamp_k.apply(lambda x:(max_timestamp - x)))


            precomp_temporal_kernel[index_n] = np.pad(time_diff, (0,num_events - i), 'constant')

            if i == size - 1:
                break

            precomp_temporal_integral[index_n] = np.pad(((time_diff_max - time_diff)/-eta), (0,num_events - i - 1), 'constant')
                                                           
    print(precomp_temporal_kernel)
    print(precomp_temporal_integral)
    print("Time taken by pre_compute_temporal_kernel is {} seconds".format((time.time() - start)))       
    return precomp_temporal_kernel, precomp_temporal_integral

# precomp_temporal_kernel, precomp_temporal_integral = pre_compute_temporal_kernel(dataframe_collection, 0.01)


# ## Influence Matrix Precomputation
# 

# In[17]:
def pre_compute_influence_matrix(sample, mu, alpha):
#    print("pre_compute_influence_matrix")   
#     start = time.time()
#     num_events = tky_checkins_df_complete.shape[0]
    influence_matrix = np.asarray(np.zeros((num_events, num_events)))
    sum_alphas_matrix = np.zeros((1, num_events - 1))
    base_intensity = np.asarray(np.zeros((num_events ,1)))    
    
#    print(len(sample))
#    print(len(missing_values_index))
#    print(missing_values_index)
    tky_checkins_df_complete['CategoryId'][missing_values_index] = sample   
#    print(tky_checkins_df_complete.CategoryId)

    def each_df(tky_checkins_df_each): 
        category_k = np.full((1, num_events), -1, dtype=int)   
        tky_checkins_df_each.reset_index(drop=True, inplace=True)
        tky_checkins_df_each.apply(influence_matrix_subfn, axis = 1, category_k=category_k)
    
    def influence_matrix_subfn(each_row, category_k):
        nonlocal influence_matrix, sum_alphas_matrix, base_intensity
        index_n = each_row['IndexRow']
        i = each_row.name
        category_k_alphas = np.zeros((1, num_events))
        category_n = each_row['CategoryId']
#        print("index_n " + str(index_n))
#        print("category_n " + str(category_n))
        base_intensity[index_n] = mu[category_n]
        category_k[0][i] = each_row['CategoryId']
        category_k_alphas = alpha[category_n,category_k]
        category_k_alphas[0][-(num_events - i):] = 0
    
        influence_matrix[index_n] = category_k_alphas
        
        if index_n != num_events - 1:            
            sum_alphas_matrix[0][index_n] = np.sum(alpha[:,category_n:category_n + 1], axis = 0)
            
    list(map(each_df, [tky_checkins_df_complete[tky_checkins_df_complete['userId'] == each] for each in userIds]))
    
                
    sum_alphas_matrix = sum_alphas_matrix*num_users

    return base_intensity, influence_matrix, sum_alphas_matrix
    


# ## Log-likelihood

# \[LL = \sum_{n=1}^N \log \lambda_{c_n}(t_n, l_n) - \sum_{c=1}^{C} \int_{T_{min}}^{T_{max}} \int_{xmin}^{xmax}\int_{ymin}^{ymax} \lambda_c(t,x,y)dydxdt \]

# In[45]:


def calculate_log_likelihood(sample, mu, alpha):
#     print("In calculate_log_likelihood")
#     start = time.time()
    first_term = 0
    second_term = 0
    global full_intensity
    
#     print("mu in likelihood fn" + str(mu))
#     print("alpha in likelihood fn " + str(alpha))
    base_intensity, influence_matrix, sum_alphas_matrix = pre_compute_influence_matrix(sample, mu, alpha)
#     print("base_intenisty " + str(base_intensity))
   
#     print("sum_alphas_matrix " + str(sum_alphas_matrix))
    ## calculate intensity
    spatio_temporal_alpha = np.multiply(influence_matrix, spatio_temporal_kernel)
#     print(spatio_temporal_alpha)
    intensity_second_term = np.sum(spatio_temporal_alpha, axis = 1).reshape((num_events,1))
    full_intensity = base_intensity + intensity_second_term
    full_intensity = np.maximum(full_intensity, 1e-20)
#     print("full intensity " + str(full_intensity))
    #full_intensity = calculate_intensity(base_intensity, influence_matrix)
    first_term = np.sum(np.log(full_intensity), axis = 0).reshape((1,1))
    
    mu_full = mu.sum()*TXY*num_users
    
    spatio_temporal_alphas = np.multiply(spatio_temporal_integral, sum_alphas_matrix)
    second_term = np.sum(spatio_temporal_alphas, axis = 1)
    second_term_full = np.sum(second_term, axis = 0)
#     print("second_term full " + str(second_term_full))
    second_term = mu_full + second_term_full
#     print("second_term " + str(second_term))
#     print("Time taken by calculate_log_likelihood is {} seconds".format((time.time() - start)))
#     print("first_term - second_term " + str(first_term - second_term))
    return first_term - second_term
        


# In[19]:


#def prior(num_unique_cat):
#    
#    #print("In prior")
#    #rv = stats.uniform(loc = 0, scale = num_unique_cat)
##     print("Prior " + str(num_unique_cat))
#    return 1/num_unique_cat
def prior(proposal_sample):
    probs = []
    counts = []
    uniq_cat_counts = dict(collections.Counter(proposal_sample))
    for each in uniq_cat_counts.keys():
        probs.append(uniq_cat_counts[each]/len(proposal_sample))
        counts.append(uniq_cat_counts[each])
    prior_value = multinomial(len(proposal_sample), probs).pmf(counts)
    return prior_value
    


# In[20]:


## returns log of posterior
def posterior(sample, mu, alpha):
    log_lik = calculate_log_likelihood(sample, mu, alpha)
    prior_value = np.log(prior(sample))
    return log_lik + prior_value


# In[41]:


def initialize_parameters(num_unique_cat):
    np.random.seed(5)
    mu = np.random.rand(num_unique_cat,1)
    alpha = np.random.rand(num_unique_cat, num_unique_cat)
    #eta = np.random.rand()
    eta = 1e0
    h = 1e0
## other h = 5*1e-4
    print("mu " + str(mu))
    print("alpha " + str(alpha))
    print("eta " + str(eta))
    print("h" + str(h))
    
    return mu, alpha, eta, h

# ## E-step

# In[39]:


def e_step(mu, alpha):
    start = time.time()
    all_samples = []
    mcmc_iter = 10000
    burning_iter = 9000
    naccept = 0
    
    if len(sys.argv) > 1 and sys.argv[1] == 'samples':
        print("------------------------loading from checkpoints----------------------------")
        samples = np.load('checkpoint_samples.npy')
        print(samples.shape)
        analyze_mcmc.analyze_mcmc_samples(samples, samples.shape[0])
        return samples

    global samples_final
    # if current_iter == 0:
    
    # else:
    #     mcmc_iter = 500
    #     burning_iter = 400
    
    samples = np.zeros((mcmc_iter - burning_iter, num_missing_cat), np.int)
    alpha = np.array([1]*(num_missing_cat*num_unique_cat)).reshape((num_missing_cat, num_unique_cat))
    
    def f(args):
        alpha[args[0], args[1]] = alpha[args[0], args[1]] + 0.1
    

    if current_iter == 0:
        old_missing = np.random.randint(low = 0, high = num_unique_cat, size = num_missing_cat)
    else:
        old_missing = samples_final
        
#    old_missing = np.random.randint(low = 0, high = num_unique_cat, size = num_missing_cat)
        
    for i in range(mcmc_iter):
           
            proposal_sample = np.random.randint(low = 0, high = num_unique_cat, size = num_missing_cat)
#            print("proposal sample {}".format(proposal_sample))            
            posterior_prop = posterior(proposal_sample, mu, alpha)
#            print("posterior from proposal sample {}".format(posterior_prop))
            
#            print("previous sample {}".format(old_missing))
            posterior_previous = posterior(old_missing, mu, alpha)
#            print("posterior from previous sample {}".format(posterior_previous))
            
            ratio = posterior_prop - posterior_previous
#            print("ratio is {}".format(ratio))
            rho = min(0, ratio)
            
            u = np.log(np.random.uniform())
            
            if u < rho: 
#                print("u " + str(u))
#                print("rho " + str(rho))
                all_samples.append(proposal_sample)
                naccept = naccept + 1
                old_missing = proposal_sample
                
            if i > burning_iter:
                samples[i-burning_iter] = old_missing
                
            if i % 1000 == 0:
                print("Number of samples accepted in this iteration is {}".format(naccept))
                print("Time taken by {}-th iteration of mcmc is {} seconds".format(i, (time.time() - start)))  
                naccept = 0
                
    # if current_iter == 0:
    print("Analyzing probability distribution of sampled category ")
    analyze_mcmc.prob_dist_categories(100, samples, actual_output, categories)
    samples = samples[[each for each in range(0, samples.shape[0]) if each%10==0], :]
    
    samples_final = stats.mode(samples)[0].reshape((num_missing_cat, ))  
    # print(samples_final)
    
    all_samples = np.array(all_samples)
    print("-----ANALYZING MCMC SAMPLES FOR ITERATION {} ".format(current_iter))
    
    
    for each in range(num_missing_cat):
        print(each)
        print(samples[:,each])
    print("Analyzing all accepted samples ...")
    analyze_mcmc.analyze_mcmc_samples(all_samples, mcmc_iter, num_unique_cat, actual_output, categories)
    print("Analyzing all accepted samples after burning period ...")
    analyze_mcmc.analyze_mcmc_samples(samples, mcmc_iter, num_unique_cat, actual_output, categories)
    print("Time taken by sampling_mcmc / e-step is {} seconds".format((time.time() - start)))
    return samples
                


# ## Jacobian 

# In[55]:


def jacobian_combined(missing_cat, mu, alpha):
   
#     start = time.time()
#    print("Entering new jacobian_combined")
    jacobian_mu = np.zeros((num_unique_cat,1), dtype = float)
    jacobian_alpha = np.zeros((num_unique_cat, num_unique_cat), dtype = float)
    global full_intensity
#    print("Intensity in JC " + str(full_intensity))
   
    tky_checkins_df_complete['CategoryId'][missing_values_index] = missing_cat  
          
    def each_df(tky_checkins_df_each): 
        category_k = np.full((1, num_events), -1, dtype=int)   
        tky_checkins_df_each.reset_index(drop=True, inplace=True)
        tky_checkins_df_each.apply(previous_cats, axis = 1, category_k=category_k, tky_checkins_df_each = tky_checkins_df_each)
    
    def summation_cat(cat_index, t, s):
        nonlocal jacobian_alpha
#         print("In summation_cat -> Before {} ".format(jacobian_alpha[:,cat_index]))
        jacobian_alpha[:,cat_index] -= t*s
#         print("In summation_cat -> After {} ".format(jacobian_alpha[:,cat_index]))
        
    def previous_cats(each_row, category_k, tky_checkins_df_each):
#         print(tky_checkins_df_each)
        nonlocal jacobian_alpha
        size = tky_checkins_df_each.shape[0]
        index_n = each_row['IndexRow']
        i = each_row.name
        
        intensity = full_intensity[index_n]
        
        if intensity <= 1e-20:
            return
        
        category_i = each_row['CategoryId'] 
        
        jacobian_mu[category_i] = jacobian_mu[category_i] + 1/intensity
        
        if i == 0:
            return
        
        category_k[0][i-1] = tky_checkins_df_each['CategoryId'][i-1]
        spatial = precomp_spatial_kernel[index_n, 0:i]
        temporal = precomp_temporal_kernel[index_n, 0:i]
        
        def f(x,y):
            jacobian_alpha[category_i][x] =  jacobian_alpha[category_i][x] + y  
        list(map(f, category_k[0, 0:i], (1/intensity)*spatial*temporal ))
            
        if i == size - 1:
            return
                
        temporal_integral_alpha = precomp_temporal_integral[index_n, 0:i] 
        spatial_integral_alpha = precomp_spatial_integral[index_n, 0:i]
        
        list(map(summation_cat, category_k[0, 0:i], temporal_integral_alpha, spatial_integral_alpha))

        
    list(map(each_df, [tky_checkins_df_complete[tky_checkins_df_complete['userId'] == each] for each in userIds]))

    jacobian_mu = (jacobian_mu) - TXY*num_users


    jacobian_mu_sqzd = np.squeeze(jacobian_mu)
    jacobian_alpha_sqzd = np.squeeze(jacobian_alpha.reshape(num_unique_cat*num_unique_cat, 1))
    jacobian_sqzd_output = np.concatenate((jacobian_mu_sqzd, jacobian_alpha_sqzd))

    
    return jacobian_sqzd_output
   


# In[24]:


def jacobian_output_subfn(sub_samples, mu, alpha):
        return np.apply_along_axis(jacobian_combined, 1, sub_samples, mu = mu, alpha = alpha)


# In[26]:


def jacobian_wrapper(x0,samples):
    print("entering jacobian wrapper")
#    print(samples)
    start = time.time()
    
    x0 = np.exp(x0)
    mu = np.asarray(x0[0:num_unique_cat]).reshape((num_unique_cat,1))
    alpha = np.asarray(x0[num_unique_cat:]).reshape((num_unique_cat,num_unique_cat))
    print(mu)
    print(alpha)
#     if not np.any(np.isfinite(mu)) or not np.any(np.isfinite(alpha)) or not np.any(mu) or not np.any(alpha):
#         return 
    
    jacob_mus = np.empty((0, num_unique_cat,1), dtype = float)
    jacob_alphas = np.empty((0, num_unique_cat, num_unique_cat), dtype = float)
    jacob_mu = np.zeros((num_unique_cat,1), dtype = float)
    jacob_alpha = np.zeros((num_unique_cat, num_unique_cat), dtype = float)
    
#     chunks = np.array_split(samples, mp.cpu_count())
    
    
    chunks = np.array_split(samples, 1)
#     print(chunks)
    pool = mp.Pool()
    jacobian_output = pool.map(partial(jacobian_output_subfn, mu = mu, alpha = alpha), chunks)
#     jacobian_output = pool.map(jacobian_output_subfn, chunks)
    pool.close()
    pool.join()
       
    def get_jacobs(each):
        nonlocal jacob_mus
        def f1(x):
            nonlocal jacob_mus
            nonlocal jacob_alphas
            a = x[num_unique_cat:].reshape((num_unique_cat,num_unique_cat))
            m = x[0:num_unique_cat].reshape((num_unique_cat,1))
            jacob_alphas = np.append(jacob_alphas, a[np.newaxis, :, :],axis=0)
            jacob_mus = np.append(jacob_mus, m[np.newaxis, :, :] , axis = 0)
        np.apply_along_axis(f1, 1, each)

    
    result = list(map(get_jacobs, jacobian_output))
    jacob_mu = np.sum(jacob_mus, axis=0)
    jacob_alpha = np.sum(jacob_alphas, axis=0)    
    jacob_mu = jacob_mu/samples.shape[0]
    jacob_alpha = jacob_alpha/samples.shape[0]
#     print("Jacobian mus in wrapper " + str(jacob_mus))
#     print("Jacobian alphas in wrapper " + str(jacob_alphas))
#     print("Jacobian mu in wrapper " + str(jacob_mu))
#     print("Jacobian alpha in wrapper " + str(jacob_alpha))
    jacob_mu_arr = np.squeeze(np.asarray(jacob_mu))
    jacob_alpha_arr = np.squeeze(np.asarray(jacob_alpha.reshape((num_unique_cat*num_unique_cat,1))))
    jacobs = np.concatenate((jacob_mu_arr, jacob_alpha_arr))
    jacobs = jacobs - (reg_param)*x0
    print("Time taken by jacobian_wrapper is {} seconds".format((time.time() - start))) 
    return -1*np.multiply(jacobs, x0)





# In[27]:


def optimization_fn(x0, samples):
    print("In optimization function")
    start = time.time()
    #print(samples)
    #print(x0.shape)
    x0 = np.exp(x0)
    
    mu = np.asarray(x0[0:num_unique_cat]).reshape((num_unique_cat,1))
    #print("mu shape " + str(mu.shape))
    alpha = np.asarray(x0[num_unique_cat:]).reshape((num_unique_cat,num_unique_cat))
#     print("values of mu and alpha in optimization fn")
    print(mu)
    print(alpha)
#     if not np.any(np.isfinite(mu)) or not np.any(np.isfinite(alpha)) or not np.any(mu) or not np.any(alpha):
#         return 
    
    value = 0
    value = np.apply_along_axis(calculate_log_likelihood, axis = 1, arr = samples, mu = mu, alpha = alpha)
#     print("value before summing in optimization fn " + str(value))
    value = sum(value)
#     print("value after summing in optimization fn " + str(value))
    
    regularisation_value = reg_param*0.5*x0.T.dot(x0)
    optimization_value = (value/samples.shape[0] - regularisation_value)*(-1)
    
    print("value " + str(value/samples.shape[0]))
    print("regularised value " + str(regularisation_value))
    print("Optimization value " + str(optimization_value))
    print("Time taken by optimization_fn is {} seconds".format((time.time() - start)))
    return optimization_value
        
        
        
        


# In[28]:


def find_missing_cat(samples, iteration_num):
    print("The missing categories learnt at iteration number " + str(iteration_num) + " is as follows")
    categories_final = stats.mode(samples)[0]
    missing_cat = []
    for each in categories_final[0]:
        missing_cat.append(categories[each])
#    print(missing_cat)
    
    return missing_cat


# In[29]:


def calculate_accuracy(missing_cat):
    correct = 0
    print("Missing cat " + str(missing_cat))
    print("Actual output " + str(actual_output))
    crosstab = pd.crosstab(pd.Series(actual_output), pd.Series(missing_cat), margins = True)
    print(crosstab)
    for each in range(len(actual_output)):
        if actual_output[each] == missing_cat[each]:
            correct = correct + 1
            
    print("Accuracy is {} %".format(correct/len(missing_cat)*100))
    


# ## Run code

# In[42]:


start_time = time.time()
num_events = tky_checkins_df_complete.shape[0]
num_users = tky_checkins_df_complete['userId'].nunique()
full_intensity = np.zeros((num_events, 1))
influence_matrix = np.asarray(np.zeros((num_events, num_events)))
sum_alphas_matrix = np.zeros((1, num_events - 1))
base_intensity = np.asarray(np.zeros((num_events ,1)))
sum_alphas_categories = np.zeros((1, num_unique_cat))
samples_final = np.empty((num_missing_cat, ))
start_opt = []
end_opt = []

mu, alpha, eta, h = initialize_parameters(num_unique_cat)

T_max = max(tky_checkins_df_complete['TimeStamp'])
T_min = min(tky_checkins_df_complete['TimeStamp'])
X_max = max(tky_checkins_df_complete['longitude'])
X_min = min(tky_checkins_df_complete['longitude'])
Y_max = max(tky_checkins_df_complete['latitude'])
Y_min = min(tky_checkins_df_complete['latitude'])    
TXY = (T_max - T_min)*(X_max - X_min)*(Y_max - Y_min)
print("values of TXY " + str(T_max) + " " + str(T_min) + " " + str(X_max) + 
      " " + str(X_min) + " " + str(Y_max) + " " + str(Y_min))
print("TXY " + str(TXY))

tky_checkins_df_complete['IndexRow'] = tky_checkins_df_complete.index
sorted_df = tky_checkins_df_complete.sort_values(['userId'])

userIds = []
dataframe_collection = []
for each in sorted_df['userId'].unique():
    df = sorted_df[sorted_df['userId'] == each]
    df.sort_values(['TimeStamp'], inplace=True)
    userIds.append(each)
    dataframe_collection.append(df)
    
precomp_spatial_kernel, precomp_spatial_integral = pre_compute_spatial_kernel(dataframe_collection, h)
precomp_temporal_kernel, precomp_temporal_integral = pre_compute_temporal_kernel(dataframe_collection, eta)
spatio_temporal_kernel = np.multiply(precomp_temporal_kernel, precomp_spatial_kernel)
spatio_temporal_integral = np.multiply(precomp_temporal_integral, precomp_spatial_integral)

spatial_kernel_filename = "SK_3_simulated" + str(h)
spatial_integral_filename = "SI_3_simulated" + str(h)
temporal_kernel_filename = "TK_simulated" + str(eta)
temporal_integral_filename = "TI_simulated" + str(eta)
np.savetxt(spatial_kernel_filename, precomp_spatial_kernel)
np.savetxt(spatial_integral_filename, precomp_spatial_integral)
np.savetxt(temporal_kernel_filename, precomp_temporal_kernel)
np.savetxt(temporal_integral_filename, precomp_temporal_integral)

#num_em_iter = 10
reg_param = 1e5
crit = 1e-5
optimized_fun_old  = 10000000
converged = False

print(reg_param)
print(crit)
# In[43]:


userIds = []
dataframe_collection = []
for each in tky_checkins_df_complete['userId'].unique():
    df = tky_checkins_df_complete[tky_checkins_df_complete['userId'] == each]
    df.sort_values(['TimeStamp'], inplace=True)
    df = df.reset_index(drop=True)
    userIds.append(each)
    dataframe_collection.append(df)


# In[ ]:


current_iter = 0
while not converged:
    
    iter_start = time.time()
    
    if (len(sys.argv) > 1) and (sys.argv[1] == 'parameters'):
        print((len(sys.argv) > 1) and (sys.argv[1] == 'parameters'))
        print("------------------------loading from checkpoints----------------------------")
        mu = np.load('checkpoint_parameter_mu.npy')
        alpha = np.load('checkpoint_parameter_alpha.npy')
    
    print(mu)
    print(alpha)    
    samples = e_step(mu, alpha)
    np.save('checkpoint_samples', samples)
    
    mu_arr = np.squeeze(np.asarray(mu))
    alpha_arr = np.squeeze(np.asarray(alpha.reshape((num_unique_cat*num_unique_cat,1))))
    
    x0 = np.concatenate((mu_arr, alpha_arr))
    x0 = np.log(x0)

    if current_iter%10 == 0:
        print("------------------Checking gradient now-------------------------- ")
        grad_test, our_grad = check_grad(optimization_fn, jacobian_wrapper, x0, samples)
        print("Grad test " + str(grad_test))
        print("Our grad {}".format(our_grad))
       
    start_opt.append(optimization_fn(x0, samples)[0][0])
    optimized_values = minimize(optimization_fn, x0, args = samples, jac = jacobian_wrapper, method = 'L-BFGS-B')
    optimized_fun_new = optimized_values.fun
    print("Optimized value in main " + str(optimized_values.fun))
    end_opt.append(optimized_values.fun[0][0])

    converged = np.abs(optimized_fun_old - optimized_fun_new)/np.abs(optimized_fun_old) < crit    
    print("Error is " + str(np.abs(optimized_fun_old - optimized_fun_new)))
    print(optimized_fun_old)
    print(optimized_fun_new)
    
    print("converged " + str(np.abs(optimized_fun_old - optimized_fun_new)/np.abs(optimized_fun_old)))
    optimized_fun_old = optimized_fun_new
    
    print("Values after optimization")
    print(optimized_values)
    print(optimized_values.x)
     
    mu_arr = np.exp(optimized_values.x[:num_unique_cat])
    alpha_arr = np.exp(optimized_values.x[num_unique_cat:])
    mu = np.asarray(mu_arr).reshape((num_unique_cat,1))
    alpha = np.asarray(alpha_arr).reshape((num_unique_cat,num_unique_cat))
    
    print("-----NEW PARAMETERS--------")
    print("mu " + str(mu))
    print("alpha " + str(alpha))
    
    np.save('checkpoint_parameter_mu', mu)
    np.save('checkpoint_parameter_alpha', alpha)
    print("Time taken for {} iteration {} seconds".format(current_iter + 1, (time.time() - iter_start)))
    
    
    print("Accuracy for this iteration")
    missing_cat = find_missing_cat(samples, current_iter)
    calculate_accuracy(missing_cat)
    
    print(start_opt)
    print(end_opt)
    
    if current_iter%3 == 0:
        analyze_mcmc.plot_optimization_values(start_opt, end_opt)
    current_iter = current_iter + 1 
    
end_time = time.time()
print("Total time taken in EM procedure is {} seconds".format((end_time - start_time)))
print("PREDICTION AND FINDING ACCURACY")
samples = e_step(mu, alpha)
missing_cat = find_missing_cat(samples, current_iter)
calculate_accuracy(missing_cat)


#all_samples = np.array(all_samples)
#np.save('all_samples', all_samples)

#def trace_samples(samples):
    
    


